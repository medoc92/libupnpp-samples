/* Copyright (c) 2018-2019 J.F. Dockes
 *
 * License: MIT/Expat
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <unistd.h>

#include <string>
#include <iostream>

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/control/typedservice.hxx"

using namespace std;
using namespace UPnPClient;
using namespace UPnPP;

class MReporter : public UPnPClient::VarEventReporter {
public:
    void changed(const char *nm, int value) {
        cerr << "Reporter: changed(char *, int) invoked for nm " << nm <<
            " ??\n";
    }
    void changed(const char *nm, const char *value)  {
        cout << "Changed: " << nm << " : " << value << endl;
    }
};

int main(int argc, char **argv)
{
    argv++;argc--;
    if (argc < 2) {
        cerr << "Usage: tpservice NameOrUid partialservicetype [action] "
            "[arg [...]]\n";
        cerr << " If no action is given, the program will just subscribe to the "
            "service events and print them forever\n";
        return 1;
    }
    string devname(*argv++);
    argc--;
    string servtp(*argv++);
    argc--;
    string actnm;
    vector<string> args;
    if (argc) {
        string actnm(*argv++);
        argc--;
        while (argc--) {
            args.push_back(*argv++);
        }
    }

    // Initialize libupnpp logging
    Logger::getTheLog("stderr")->setLogLevel(Logger::LLDEB1);
    // Explicitely initialize libupnpp so that we can display a
    // possible error
    LibUPnP *mylib = LibUPnP::getLibUPnP();
    if (!mylib) {
        cerr << "Can't get LibUPnP" << endl;
        return 1;
    }
    if (!mylib->ok()) {
        cerr << "Lib init failed: " <<
            mylib->errAsString("main", mylib->getInitError()) << endl;
        return 1;
    }

    TypedService *srv = findTypedService(devname, servtp, true);

    if (!srv) {
        cerr << "Service " << devname << "/" << servtp << " not found" << endl;
        return 1;
    }

    if (!actnm.empty()) {
        map<string, string> data;
        int ret = srv->runAction(actnm, args, data);
        if (ret == 0) {
            for (auto& entry: data) {
                cout << entry.first << "->" << entry.second << endl;
            }
        } else {
            cerr << "runAction failed with code " << ret << endl;
            return 1;
        }
    }
    MReporter reporter;
    srv->installReporter(&reporter);
    sleep(1000);
    return 0;
}
