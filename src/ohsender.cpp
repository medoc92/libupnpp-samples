/* Copyright (c) 2018-2019 J.F. Dockes
 *
 * License: MIT/Expat
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Code to exercise the libupnpp OhSender class

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/log.hxx"
#include "libupnpp/upnpputils.hxx"
#include "libupnpp/control/cdirectory.hxx"
#include "libupnpp/control/discovery.hxx"
#include "libupnpp/control/mediarenderer.hxx"
#include "libupnpp/control/ohsender.hxx"
#include "libupnpp/control/linnsongcast.hxx"

using namespace std;
using namespace UPnPClient;
using namespace UPnPP;

class MReporter : public UPnPClient::VarEventReporter {
public:
    void changed(const char *nm, int value) {
        cout << "Changed: " << nm << " : " << value << endl;
    }
    void changed(const char *nm, const char *value)  {
        cout << "Changed: " << nm << " : " << value << endl;
    }

    void changed(const char *nm, UPnPDirObject meta) {
        cout << "Changed: " << nm << " : " << meta.dump() << endl;
    }
};

void rdMonitor(OHSNH hdl)
{
    MReporter reporter;
    hdl->installReporter(&reporter);

    while (true) {
        sleep(2);
        string uri;
        string meta;
        int ret;
        if ((ret = hdl->metadata(uri, meta)) == 0) {
            cout << "Uri: " << uri << " metadata " << meta << endl;
        } else {
            cerr << "Metadata: failed: " << ret << endl;
        }
    }
}

void metadata(OHSNH hdl)
{
    string uri, meta;
    int ret;
    if ((ret = hdl->metadata(uri, meta)) != 0) {
        cerr << "metadata failed: " << ret << endl;
        return;
    }
    cout << "read: uri: [" << uri << "] meta: " << meta << endl;
}

static char *thisprog;
static char usage [] =
" -M <renderer>: monitor OHSender\n"
" -m <renderer>: run metadata\n"
" \n"
;

static void
Usage(void)
{
    fprintf(stderr, "%s: usage:\n%s", thisprog, usage);
    exit(1);
}
static int	   op_flags;
#define OPT_M    0x1
#define OPT_m    0x2

static struct option long_options[] = {
    {0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
    string fname;
    string arg;

    thisprog = argv[0];

    int ret;
    int option_index = 0;
    while ((ret = getopt_long(argc, argv, "Mm", 
                              long_options, &option_index)) != -1) {
        switch (ret) {
        case 'M': if (op_flags) Usage(); op_flags |= OPT_M; break;
        case 'm': if (op_flags) Usage(); op_flags |= OPT_m; break;
        default:
            Usage();
        }
    }
    if (!op_flags)
        Usage();
    
    if (op_flags & (OPT_M|OPT_m)) {
            if (optind != argc - 1) 
                Usage();
            fname = argv[optind++];
    }
            
    if (Logger::getTheLog("/tmp/ohsender.log") == 0) {
        cerr << "Can't initialize log" << endl;
        return 1;
    }
    Logger::getTheLog("")->setLogLevel(Logger::LLDEB1);

    LibUPnP *mylib = LibUPnP::getLibUPnP();
    if (!mylib) {
        cerr << "Can't get LibUPnP" << endl;
        return 1;
    }

    if (!mylib->ok()) {
        cerr << "Lib init failed: " <<
            mylib->errAsString("main", mylib->getInitError()) << endl;
        return 1;
    }
    mylib->setLogFileName("/tmp/libupnp.log", LibUPnP::LogLevelDebug);

    string reason;
    OHSNH hdl = Songcast::getSender(fname, reason);
    if (!hdl) {
        cerr << "Device has no OpenHome Sender service" << endl;
        return 1;
    }
    
    if ((op_flags & OPT_M)) {
        rdMonitor(hdl);
    } else if ((op_flags & OPT_m)) {
        metadata(hdl);
    } else {
        Usage();
    }

    return 0;
}
