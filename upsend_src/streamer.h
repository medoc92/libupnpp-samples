/* Copyright (c) 2018-2019 J.F. Dockes
 *
 * License: MIT/Expat
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef _STREAMER_H_INCLUDED_
#define _STREAMER_H_INCLUDED_

#include "workqueue.h"

#include <unordered_map>

// The audio messages which get passed between the reader and the http
// server part.
class AudioMessage {
public:
    // buf is is a malloced buffer, and we take ownership. The caller
    // MUST NOT free it. Bytes is data count, allocbytes is the buffer size.
    AudioMessage(char *buf, size_t bytes, size_t allocbytes) 
        : m_bytes(bytes), m_allocbytes(allocbytes), m_buf(buf), m_curoffs(0) {
    }

    ~AudioMessage() {
        if (m_buf)
            free(m_buf);
    }
    unsigned int m_bytes; // Useful bytes
    unsigned int m_allocbytes; // buffer size
    char *m_buf;
    unsigned int m_curoffs; /* Used by the http data emitter */
};

class AudioSink {
public:
    struct Context {
        Context(WorkQueue<AudioMessage*> *q)
            : queue(q), config(0), filesize(0) {
        }
        WorkQueue<AudioMessage*> *queue;
        std::unordered_map<std::string,std::string> config;
        std::string filename;
        std::string content_type;
        std::string ext;
        off_t filesize;
    };

    AudioSink(void *(*w)(Context *))
        : worker(w) {
    }

    /** Worker routine for fetching bufs from the rcvqueue and sending them
     * further. The param is actually an AudioSink::Context */
    void *(*worker)(Context *);
};

extern AudioSink httpAudioSink;

#endif /* _STREAMER_H_INCLUDED_ */
