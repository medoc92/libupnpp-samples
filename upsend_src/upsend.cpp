/* Copyright (c) 2018-2019 J.F. Dockes
 *
 * License: MIT/Expat
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <strings.h>

#include <string>
#include <iostream>

#include "libupnpp/upnpplib.hxx"
#include "libupnpp/log.hxx"
#include "libupnpp/soaphelp.hxx"
#include "libupnpp/upnpputils.hxx"
#include "libupnpp/control/mediarenderer.hxx"
#include "libupnpp/control/avtransport.hxx"
#include "libupnpp/control/discovery.hxx"

#include "streamer.h"
#include "wav.h"

using namespace std;
using namespace UPnPClient;
using namespace UPnPP;

WorkQueue<AudioMessage*> audioqueue("audioqueue", 4);

// @param name can be uuid or friendly name, we try both. The chance that a
//     device would have a uuid which would be the friendly name of  another is small...
MRDH getRenderer(const string& name)
{
    static UPnPDeviceDirectory *superdir;
    if (superdir == 0) {
        superdir = UPnPDeviceDirectory::getTheDir();
        if (superdir == 0) {
            std::cerr << "Discovery init failed\n";
            return MRDH();
        }
    }
    
    UPnPDeviceDesc ddesc;
    if (superdir->getDevByFName(name, ddesc)) {
        return MRDH(new MediaRenderer(ddesc));
    } else if (superdir->getDevByUDN(name, ddesc)) {
        return MRDH(new MediaRenderer(ddesc));
    }
    std::cerr << "Can't connect to " << name << "\n";
    return MRDH();
}

static string path_suffix(const string& s)
{
    string::size_type dotp = s.rfind('.');
    if (dotp == string::npos) {
        return string();
    }
    return s.substr(dotp + 1);
}

static bool whatfile(const string& audiofile, AudioSink::Context *ctxt)
{
    if (access(audiofile.c_str(), R_OK) != 0) {
        std::cerr << "No read access " << audiofile << " errno " << errno << "\n";
        return false;
    }
    struct stat st;
    if (stat(audiofile.c_str(), &st)) {
        std::cerr << "Can't stat " << audiofile << " errno " << errno << "\n";
        return false;
    }

    string ext = path_suffix(audiofile);
    ctxt->filename = audiofile;
    ctxt->filesize = st.st_size;
    ctxt->ext = ext;
    const char* cext = ext.c_str();
    if (!strcasecmp("flac", cext)) {
        ctxt->content_type = "audio/flac";
    } else if (!strcasecmp("mp3", cext)) {
        ctxt->content_type = "audio/mpeg";
    } else if (!strcasecmp("wav", cext)) {
        ctxt->content_type = "audio/wav";
    } else {
        std::cerr << "Unknown extension " << ext << "\n";
        return false;
    }
    return true;
}

void *readworker(void *a)
{
    AudioSink::Context *ctxt = (AudioSink::Context *)a;

    int fd = 0;
    if (ctxt->filename.compare("stdin")) {
        if ((fd = open(ctxt->filename.c_str(), O_RDONLY)) < 0) {
            std::cerr << "readWorker: open " << ctxt->filename << " : errno " << errno << "\n";
            exit(1);
        }
    }

    for (;;) {
        unsigned int allocbytes = 4096;
        char *buf = (char *)malloc(allocbytes);
        if (buf == 0) {
            std::cerr << "readWorker: can't allocate " << allocbytes << " bytes\n";
            exit(1);
        }
        ssize_t readbytes = read(fd, buf, allocbytes);
        //std::cerr << "readworker: got " << readbytes << "bytes\n";
        if (readbytes < 0) {
            std::cerr << "readWorker: read " << ctxt->filename << " errno " << errno << "\n";
            exit(1);
        } else if (readbytes == 0) {
            std::cerr << "readworker: EOF\n";
            audioqueue.waitIdle();
            audioqueue.setTerminateAndWait();
            return nullptr;
        }
        AudioMessage *ap = new AudioMessage(buf, readbytes, allocbytes);
        if (!audioqueue.put(ap, false)) {
            std::cerr << "readWorker: queue dead: exiting\n";
            exit(1);
        }
    }
}

string didlmake(const string& uri, const string& mime)
{
    ostringstream ss;
    ss << "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
       "<DIDL-Lite xmlns:dc=\"http://purl.org/dc/elements/1.1/\" "
       "xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\" "
       "xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" "
       "xmlns:dlna=\"urn:schemas-dlna-org:metadata-1-0/\">"
       << "<item restricted=\"1\" parentID=\"0\" id=\"1234\">";

    ss << "<dc:title>" << SoapHelp::xmlQuote("Upsend Streaming") << "</dc:title>";
    ss << "<upnp:class>object.item.audioItem.musicTrack</upnp:class>";

    ss << "<res " <<
// Too lazy to stat the audio file and most renderers will work without these.
//        "duration=\"" << upnpduration(30) << "\" " << 
//        "sampleFrequency=\"44100\" audioChannels=\"2\" " << 
        "protocolInfo=\"http-get:*:" << mime << ":*\"" << ">" <<
        SoapHelp::xmlQuote(uri) <<
        "</res></item></DIDL-Lite>";
    return ss.str();
}

static char *thisprog;
static char usage [] = "<audiofile> <renderer> : play audio on given UPnP/AV renderer\n"
    "if <audiofile> is 'stdin', will expect and read a raw 44100/16/2 stream from stdin\n"
    ;
static void Usage(void)
{
    fprintf(stderr, "%s: usage:\n%s", thisprog, usage);
    exit(1);
}
static int	   op_flags;
#define OPT_h    0x1
#define OPT_p    0x2
static struct option long_options[] = {
    {"host", required_argument, 0, 'h'},
    {"port", required_argument, 0, 'p'},
    {0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
    thisprog = argv[0];
    string host;
    string port = "8869";
    
    int option_index = 0;
    int ret;
    while ((ret = getopt_long(argc, argv, "h:p:", 
                              long_options, &option_index)) != -1) {
        cerr << "ret is " << ret << endl;
        switch (ret) {
        case 'h': op_flags |= OPT_h; host = optarg; break;
        case 'p': op_flags |= OPT_h; port = optarg; break;
        default:  Usage();
        }
    }
    if (optind != argc - 2)
        Usage();
    string audiofile = argv[optind++];
    string renderer = argv[optind++];
            
    if (Logger::getTheLog("stderr") == 0) {
        std::cerr << "Can't initialize log" << "\n";
        return 1;
    }
    Logger::getTheLog("")->setLogLevel(Logger::LLDEB1);

    string hwa;
    LibUPnP *mylib = LibUPnP::getLibUPnP(false, &hwa);
    if (!mylib) {
        std::cerr << "Can't get LibUPnP" << "\n";
        return 1;
    }
    if (!mylib->ok()) {
        std::cerr << "Lib init failed: " <<
            mylib->errAsString("main", mylib->getInitError()) << "\n";
        return 1;
    }

    if (host.empty()) 
        host = mylib->host();

    if (host.empty()) {
        std::cerr << "Can't retrieve lib IP address or port\n";
        return 1;
    }
    //mylib->setLogFileName("/tmp/libupnp.log", LibUPnP::LogLevelDebug);
    std::cerr << "Using " << host << ":" << port << "\n";
    MRDH rdr = getRenderer(renderer);
    if (!rdr) {
        std::cerr << "Can't connect to renderer\n";
        return 1;
    }
    AVTH avth = rdr->avt();
    if (!avth) {
        std::cerr << "Device has no AVTransport service" << "\n";
        return 1;
    }
    
    // Identify file
    AudioSink::Context *ctxt = new AudioSink::Context(&audioqueue);
    bool makewav = false;
    if (!audiofile.compare("stdin")) {
        ctxt->filename = audiofile;
        ctxt->ext = "wav";
        ctxt->content_type = "audio/wav";
        makewav = true;
    } else {
        if (!whatfile(audiofile, ctxt)) {
            std::cerr << "Can't identify file " << audiofile << "\n";
            return 1;
        }
    }

    unordered_map<string,string> c{{"httpport", port}, {"httphost", host}};
    ctxt->config = c;
    
    // Start the http thread
    audioqueue.start(1, (void *(*)(void *))(httpAudioSink.worker), ctxt);

    if (makewav) {
        unsigned int allocbytes = 512;
        char *buf = (char *)malloc(allocbytes);
        if (buf == 0) {
            std::cerr << "Can't allocate " << allocbytes << " bytes\n";
            exit(1);
        }
        int freq = 44100;
        int bits = 16;
        int chans = 2;
        int databytes = 2 * 1000 * 1000 * 1000;
        // Using buf+bytes in case we ever insert icy before the audio
        int sz = makewavheader(buf, allocbytes, freq, bits, chans, databytes);
        AudioMessage *ap = new AudioMessage(buf, sz, allocbytes);
        audioqueue.put(ap, false);
    }
    
    // Start the reading thread
    std::thread readthread(readworker, ctxt);

    string uri("http://" + host + ":" + port + "/stream." + ctxt->ext);

    // We'd need a few options here to decide what to do if already playing:
    // wait (would allow to queue multiple songs), or interrupt.

    // Start the renderer
    if (avth->setAVTransportURI(uri, didlmake(uri, ctxt->content_type)) != 0) {
        std::cerr << "setAVTransportURI failed\n";
        return 1;
    }

    if (avth->play() != 0) {
        std::cerr << "Play failed\n";
        return 1;
    }
    readthread.join();
    return 0;
}
